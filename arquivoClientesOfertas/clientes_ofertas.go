package arquivoClientesOfertas

type ClientesOfertas struct {
	CodigoOferta int32  `json:"CodigoOferta"`
	CnpjCliente  string `json:"CnpjCliente"`
}
