package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Rodape struct {
	TipoRegistro       int32  `json:"TipoRegistro"`
	NumeroPedido       string `json:"NumeroPedido"`
	QuantidadeItens    int32  `json:"QuantidadeItens"`
	QuantidadeUnidades int32  `json:"QuantidadeUnidades"`
}

func (r *Rodape) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRodape

	err = posicaoParaValor.ReturnByType(&r.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.NumeroPedido, "NumeroPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.QuantidadeItens, "QuantidadeItens")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.QuantidadeUnidades, "QuantidadeUnidades")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRodape = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":       {0, 1, 0},
	"NumeroPedido":       {1, 21, 0},
	"QuantidadeItens":    {21, 26, 0},
	"QuantidadeUnidades": {26, 36, 0},
}
