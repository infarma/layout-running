package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Detalhe struct {
	TipoRegistro      int32   `json:"TipoRegistro"`
	NumeroPedido      string  `json:"NumeroPedido"`
	Ean13             string  `json:"Ean13"`
	Quantidade        int32   `json:"Quantidade"`
	CondicaoComercial int32   `json:"CondicaoComercial"`
	Desconto          float32 `json:"Desconto"`
	Prazo             int32   `json:"Prazo"`
	CondicaoDesconto  string  `json:"CondicaoDesconto"`
	CondicaoPrazo     string  `json:"CondicaoPrazo"`
	CodigoPrazo       string  `json:"CodigoPrazo"`
	CodigoOferta      string  `json:"CodigoOferta"`
	Preco             float32 `json:"Preco"`
}

func (d *Detalhe) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesDetalhe

	err = posicaoParaValor.ReturnByType(&d.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.NumeroPedido, "NumeroPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Ean13, "Ean13")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Quantidade, "Quantidade")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.CondicaoComercial, "CondicaoComercial")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Desconto, "Desconto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Prazo, "Prazo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.CondicaoDesconto, "CondicaoDesconto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.CondicaoPrazo, "CondicaoPrazo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.CodigoPrazo, "CodigoPrazo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.CodigoOferta, "CodigoOferta")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Preco, "Preco")
	if err != nil {
		return err
	}

	return err
}

var PosicoesDetalhe = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":      {0, 1, 0},
	"NumeroPedido":      {1, 21, 0},
	"Ean13":             {21, 34, 0},
	"Quantidade":        {34, 39, 0},
	"CondicaoComercial": {39, 43, 0},
	"Desconto":          {43, 48, 2},
	"Prazo":             {48, 51, 0},
	"CondicaoDesconto":  {51, 52, 0},
	"CondicaoPrazo":     {52, 53, 0},
	"CodigoPrazo":       {53, 93, 0},
	"CodigoOferta":      {93, 113, 0},
	"Preco":             {113, 120, 2},
}
