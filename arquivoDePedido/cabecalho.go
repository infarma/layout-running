package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Cabecalho struct {
	TipoRegistro               int32  `json:"TipoRegistro"`
	CnpjFarmacia               string `json:"CnpjFarmacia"`
	NumeroPedido               string `json:"NumeroPedido"`
	DataPedido                 int32  `json:"DataPedido"`
	CondicaoPagamento          string `json:"CondicaoPagamento"`
	PrazoPagamento             int32  `json:"PrazoPagamento"`
	TipoCompra                 string `json:"TipoCompra"`
	TipoRetorno                string `json:"TipoRetorno"`
	IndentificacaoLinhaProduto string `json:"IndentificacaoLinhaProduto"`
	CompraIntegral             string `json:"CompraIntegral"`
	CnpjDistribuidor           string `json:"CnpjDistribuidor"`
	NumeroOS                   string `json:"NumeroOS"`
	Observacao                 string `json:"Observacao"`
	CodgoRepresentante         string `json:"CodgoRepresentante"`
	CodigoCampanha             string `json:"CodigoCampanha"`
	FaturamentoIntegral        int32  `json:"FaturamentoIntegral"`
}

func (c *Cabecalho) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesCabecalho

	err = posicaoParaValor.ReturnByType(&c.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CnpjFarmacia, "CnpjFarmacia")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NumeroPedido, "NumeroPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.DataPedido, "DataPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CondicaoPagamento, "CondicaoPagamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.PrazoPagamento, "PrazoPagamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.TipoCompra, "TipoCompra")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.TipoRetorno, "TipoRetorno")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.IndentificacaoLinhaProduto, "IndentificacaoLinhaProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CompraIntegral, "CompraIntegral")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CnpjDistribuidor, "CnpjDistribuidor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NumeroOS, "NumeroOS")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.Observacao, "Observacao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CodgoRepresentante, "CodgoRepresentante")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CodigoCampanha, "CodigoCampanha")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.FaturamentoIntegral, "FaturamentoIntegral")
	if err != nil {
		return err
	}

	return err
}

var PosicoesCabecalho = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":               {0, 1, 0},
	"CnpjFarmacia":               {1, 16, 0},
	"NumeroPedido":               {16, 36, 0},
	"DataPedido":                 {36, 44, 0},
	"CondicaoPagamento":          {44, 45, 0},
	"PrazoPagamento":             {45, 47, 0},
	"TipoCompra":                 {47, 48, 0},
	"TipoRetorno":                {48, 49, 0},
	"IndentificacaoLinhaProduto": {49, 50, 0},
	"CompraIntegral":             {50, 51, 0},
	"CnpjDistribuidor":           {51, 66, 0},
	"NumeroOS":                   {66, 86, 0},
	"Observacao":                 {86, 186, 0},
	"CodgoRepresentante":         {186, 196, 0},
	"CodigoCampanha":             {196, 202, 0},
	"FaturamentoIntegral":        {202, 203, 0},
}
