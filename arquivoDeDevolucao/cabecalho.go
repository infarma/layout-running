package arquivoDeDevolucao

type Cabecalho struct {
	TipoRegistro        int32  `json:"TipoRegistro"`
	DataGeracaoArquivo  int32  `json:"DataGeracaoArquivo"`
	HoraGeracaoArquivo  int32  `json:"HoraGeracaoArquivo"`
	CnpjDistribuidor    string `json:"CnpjDistribuidor"`
	CodigoProjeto       string `json:"CodigoProjeto"`
	NumeroPedido        string `json:"NumeroPedido"`
	NumeroPedidoCliente string `json:"NumeroPedidoCliente"`
	Livre               string `json:"Livre"`
}
