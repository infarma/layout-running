package arquivoDeDevolucao

type ItensNotaFiscal struct {
	TipoRegistro              int32   `json:"TipoRegistro"`
	CodigoEan                 string  `json:"CodigoEan"`
	CodigoProdutoDistribuidor string  `json:"CodigoProdutoDistribuidor"`
	Quantidade                int32   `json:"Quantidade"`
	Unidade                   string  `json:"Unidade"`
	PrecoFabrica              float32 `json:"PrecoFabrica"`
	DescontoComercial         float32 `json:"DescontoComercial"`
	ValorDesconto             float32 `json:"ValorDesconto"`
	ValorRepasse              float32 `json:"ValorRepasse"`
	Repasse                   float32 `json:"Repasse"`
	ValorUnitario             float32 `json:"ValorUnitario"`
	Facionamento              int32   `json:"Facionamento"`
	MotivoDevolucao           int32   `json:"MotivoDevolucao"`
	Livre                     string  `json:"Livre"`
}
