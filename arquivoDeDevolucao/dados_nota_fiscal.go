package arquivoDeDevolucao

type DadosNotaFiscal struct {
	TipoRegistro          int32  `json:"TipoRegistro"`
	DataEntradaMercadoria int32  `json:"DataEntradaMercadoria"`
	HoraEntradaMercadoria int32  `json:"HoraEntradaMercadoria"`
	DataEmissaoNotaFiscal int32  `json:"DataEmissaoNotaFiscal"`
	CnpjLoja              string `json:"CnpjLoja"`
	NumeroNotaFiscal      string `json:"NumeroNotaFiscal"`
	SerieDocumento        string `json:"SerieDocumento"`
	Livre                 string `json:"Livre"`
}
