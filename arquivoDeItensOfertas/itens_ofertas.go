package arquivoDeItensOfertas

type ItensOfertas struct {
	CodigoOferta   int32   `json:"CodigoOferta"`
	Ean13          string  `json:"Ean13"`
	Uf             string  `json:"Uf"`
	Preco          float32 `json:"Preco"`
	Prazo          int32   `json:"Prazo"`
	CodigoPrazo    int32   `json:"CodigoPrazo"`
	DescricaoPrazo int32   `json:"DescricaoPrazo"`
	Desconto       float32 `json:"Desconto"`
}
