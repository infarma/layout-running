## Arquivo de Pedido
 gerador-layouts arquivoDePedido cabecalho TipoRegistro:int32:0:1 CnpjFarmacia:string:1:16 NumeroPedido:string:16:36 DataPedido:int32:36:44 CondicaoPagamento:string:44:45 PrazoPagamento:int32:45:47 TipoCompra:string:47:48 TipoRetorno:string:48:49 IndentificacaoLinhaProduto:string:49:50 CompraIntegral:string:50:51 CnpjDistribuidor:string:51:66 NumeroOS:string:66:86 Observacao:string:86:186 CodgoRepresentante:string:186:196 CodigoCampanha:string:196:202 FaturamentoIntegral:int32:202:203
 
 gerador-layouts arquivoDePedido detalhe TipoRegistro:int32:0:1 NumeroPedido:string:1:21 Ean13:string:21:34 Quantidade:int32:34:39 CondicaoComercial:int32:39:43 Desconto:float32:43:48:2 Prazo:int32:48:51 CondicaoDesconto:string:51:52 CondicaoPrazo:string:52:53 CodigoPrazo:string:53:93 CodigoOferta:string:93:113 Preco:float32:113:120:2
 
 gerador-layouts arquivoDePedido rodape TipoRegistro:int32:0:1 NumeroPedido:string:1:21 QuantidadeItens:int32:21:26 QuantidadeUnidades:int32:26:36
 

## Arquivo de Retorno
 gerador-layouts arquivoDeRetorno cabecalho TipoRegistro:int32:0:1 CnpjFarmacia:string:1:16 NumeroPedido:string:16:36 DataProcessamento:int32:36:44 CondicaoPagamento:string:44:45 HoraProcessamento:int32:45:53 NumeroPedido:int32:53:65 SituacaoFechamento:int32:65:67 DataVencimento:int32:67:75 CnpjDistribuidor:string:75:90
 
 gerador-layouts arquivoDeRetorno detalhe TipoRegistro:int32:0:1 Gtin13:string:1:14 NumeroPedido:string:14:34 QuantidadeAtendida:int32:34:39 DescontoAplicado:float32:39:44:2 PrazoConcedido:int32:44:47 QuantidadeNaoAntendida:int32:47:52 Motivo:string:52:102 NumeroNotaFiscal:string:102:114 SerieNotaFiscal:string:114:117 DataEmissao:int32:117:125 ChaveAcessoNotaFiscal:string:125:169
 

## Arquivo de Nota Fiscal
 gerador-layouts arquivoNotaFiscal cabecalho TipoRegistro:int32:0:1 CnpjFarmacia:string:1:16 NumeroPedidoFornecedor:string:16:36 DataFaturamento:int32:36:44 CondicaoFaturamento:string:44:45 HoraFaturamento:int32:45:53 NumeroPedidoDistribuidor:string:53:65 SituacaoFechamento:int32:65:67 DataVencimento:int32:67:75 CnpjDistribuidor:string:75:90
 
 gerador-layouts arquivoNotaFiscal detalhe TipoRegistro:int32:0:1 Ean13:string:1:14 NumeroPedido:string:14:34 QuantidadeAtendida:int32:34:39 DescontoAplicado:float32:39:44:2 PrazoConcedido:int32:44:47 QuantidadeNaoAtendida:int32:47:52 Motivo:string:52:102 NumeroNotaFiscal:string:102:114 SerieNotaFiscal:string:114:117 DataEmissaoNotaFiscal:int32:117:125 ChaveAcessoNotaFiscal:string:125:169 ValorLiquidoFaturado:float32:169:179:2 ValorSubstituicaoTributaria:float32:179:189:2
 
 gerador-layouts arquivoNotaFiscal rodape TipoRegistro:int32:0:1 NumeroPedido:string:1:21 QuantidadeLinhas:int32:21:26 QuantidadeItensAtendidos:int32:26:31 QuantidadeItensNaoAtendidos:int32:31:36 LimiteDisponivel:float32:36:50:2
 
 
## Arquivo de Devolucao
 gerador-layouts arquivoDeDevolucao cabecalho TipoRegistro:int32:0:1 DataGeracaoArquivo:int32:1:9 HoraGeracaoArquivo:int32:9:15 CnpjDistribuidor:string:15:29 CodigoProjeto:string:29:30 NumeroPedido:string:30:50 NumeroPedidoCliente:string:50:65 Livre:string:65:93
 
 gerador-layouts arquivoDeDevolucao dadosNotaFiscal TipoRegistro:int32:0:1 DataEntradaMercadoria:int32:1:9 HoraEntradaMercadoria:int32:9:15 DataEmissaoNotaFiscal:int32:15:23 CnpjLoja:string:23:37 NumeroNotaFiscal:string:37:49 SerieDocumento:string:49:52 Livre:string:52:78
 
 gerador-layouts arquivoDeDevolucao itensNotaFiscal TipoRegistro:int32:0:1 CodigoEan:string:1:14  CodigoProdutoDistribuidor:string:14:21 Quantidade:int32:21:25 Unidade:string:25:28 PrecoFabrica:float32:28:36:2 DescontoComercial:float32:36:40:2 ValorDesconto:float:40:48:2 ValorRepasse:float32:48:56:2 Repasse:float32:56:60:2 ValorUnitario:float32:60:68:2 Facionamento:int32:68:72 MotivoDevolucao:int32:72:74 Livre:string:74:100
 
 gerador-layouts arquivoDeDevolucao fimArquivo TipoRegistro:int32:0:1 NumeroItensNotaFiscal:int32:1:5 Livre:string:5:80
 
  
## Arquvo de Ofertas
 gerador-layouts arquivoDeOfertas cabecalho CodigoOferta:int32:0:5 DescricaoOferta:string:5:35 DataInicioOferta:int32:35:43 HoraInicioOferta:int32:43:47 DataFimOferta:int32:47:55 HoraFimOferta:int32:55:59 ValorMinimo:int32:59:65 QuantidadeMinima:int32:65:71
 
 
## Arquivo de Itens das Ofertas 
 gerador-layouts arquivoDeItensOfertas itensOfertas CodigoOferta:int32:0:5 Ean13:string:5:18 Uf:string:18:20 Preco:float32:20:27:2 Prazo:int32:27:31 CodigoPrazo:int32:31:36 DescricaoPrazo:int32:36:76 Desconto:float32:76:81:2
 
 
## Arquivo de Clientes das Ofertas
 gerador-layouts arquivoClientesOfertas clientesOfertas CodigoOferta:int32:0:5 CnpjCliente:string:5:19
 
 
## Arquivo de Estoque do Distribuidor  
 gerador-layouts arquivoEstoqueDistribuidor cabecalho TipoRegistro:int32:0:1 CnpjDistribuidor:string:1:15 Data:int32:15:23 Hora:int32:23:27
 
 gerador-layouts arquivoEstoqueDistribuidor cabecalho TipoRegistro:int32:0:1 Ean13:string:1:14 Quantidade:int32:14:29
 
 
## Arquivo de Restricoes
 gerador-layouts arquivoDeRestricoes restricoes TipoRestricao:int32:0:1 DataConsulta:int32:1:13 CnpjCliente:string:13:27 DescricaoDocumentacao:string:27:127 ClienteDesde:int32:127:135 NumetoTitulo:string:135:150 ValorTitulo:float32:150:159:2 DataVencimento:int32:159:167 DiasEmAtraso:int32:167:171 LimiteCredito:float32:171:180:2 ValorAtraso:float32:180:189:2 ValorAcumulado:float32:189:198:2 DataUltimaCompra:int32:198:206 ValorMinimoPedido:float32:206:215:2
 
 
## Arquivo de Cancelamento
 gerador-layouts arquivoDeCancelamento distribuidor TipoRegistro:int32:0:1 CnpjDistribuidor:string:1:15
 
 gerador-layouts arquivoDeCancelamento cliente TipoRegistro:int32:0:1 CnpjCliente:string:1:15
 
 gerador-layouts arquivoDeCancelamento pedido TipoRegistro:int32:0:1 NumeroPedido:string:1:21
 
 gerador-layouts arquivoDeCancelamento itens TipoRegistro:int32:0:1 Vago:string:1:3 CodigoEan:string:3:16 Quantidade:int32:16:24 Motivo:string:24:74
 
 gerador-layouts arquivoDeCancelamento data TipoRegistro:int32:0:1 Data:string:1:9 Hora:string:9:15 
 
 
## Arquivo de Importacao de Pedidos de Grandes Redes
 gerador-layouts arquivoImportacaoPedidosGrandesRedes cabecalho TipoRegistro:int32:0:1 Vago:string:1:2 CnpjFarmacia:string:2:16 NumeroPedidoIndustria:string:16:28 DataProcessamento:int32:28:36 HoraProcessamento:int32:36:44 NumeroPedidoDistribuidor:string:44:56 Motivo:string:56:58 CnpjCDFaturamento:string:58:72 NumeroOferta:int32:72:77 PrazoOferta:int32:77:80 ChaveNotaFiscal:string:80:124 DescricaoOferta:string:124:154
 
 gerador-layouts arquivoImportacaoPedidosGrandesRedes itens TipoRegistro:int32:0:1 CodigoProduto:string:1:14 NumeroPedidoIndustria:string:14:26 CodigoPagamento:string:26:27 QuantidadeAtendida:int32:27:32 DescontoAplicado:int32:32:37 PrecoUnitario:float32:37:49:2 PrecoTotalProdutos:float32:49:61:2 Vago:string:61:64 QuantidadeNaoAtendida:int32:64:69 Motivo:string:69:90 CodigoOferta:int32:90:95 DescricaoOferta:string:95:125
 
 gerador-layouts arquivoImportacaoPedidosGrandesRedes lote TipoRegistro:int32:0:1 EspecieDocumento:string:1:4 SerieNotaFiscal:string:4:7 NumeroNotaFiscal:int32:7:13 NumeroLote:string:13:28 DataVencimentoLote:int32:28:36 QuantidadeLote:int32:36:43
 
 gerador-layouts arquivoImportacaoPedidosGrandesRedes trailler TipoRegistro:int32:0:1 NumeroPedido:string:1:13 QuantidadeLinhas:int32:13:18 QuantidadeAtendida:int32:18:23 QuantidadeNaoAtendida:in32:23:28 NumeroNotaFiscal:string:28:36 DataEmissaoNota:int32:36:44
 
 gerador-layouts arquivoImportacaoPedidosGrandesRedes resumoArquivo TipoRegistro:int32:0:1 QuantidadeNotaFiscal:string:1:13 ValorTotalNotas:float32:13:27:2 