package arquivoDeOfertas

type Cabecalho struct {
	CodigoOferta     int32  `json:"CodigoOferta"`
	DescricaoOferta  string `json:"DescricaoOferta"`
	DataInicioOferta int32  `json:"DataInicioOferta"`
	HoraInicioOferta int32  `json:"HoraInicioOferta"`
	DataFimOferta    int32  `json:"DataFimOferta"`
	HoraFimOferta    int32  `json:"HoraFimOferta"`
	ValorMinimo      int32  `json:"ValorMinimo"`
	QuantidadeMinima int32  `json:"QuantidadeMinima"`
}
