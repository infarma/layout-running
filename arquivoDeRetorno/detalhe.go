package arquivoDeRetorno

type Detalhe struct {
	TipoRegistro           int32   `json:"TipoRegistro"`
	Gtin13                 string  `json:"Gtin13"`
	NumeroPedido           string  `json:"NumeroPedido"`
	QuantidadeAtendida     int32   `json:"QuantidadeAtendida"`
	DescontoAplicado       float32 `json:"DescontoAplicado"`
	PrazoConcedido         int32   `json:"PrazoConcedido"`
	QuantidadeNaoAntendida int32   `json:"QuantidadeNaoAntendida"`
	Motivo                 string  `json:"Motivo"`
	NumeroNotaFiscal       string  `json:"NumeroNotaFiscal"`
	SerieNotaFiscal        string  `json:"SerieNotaFiscal"`
	DataEmissao            int32   `json:"DataEmissao"`
	ChaveAcessoNotaFiscal  string  `json:"ChaveAcessoNotaFiscal"`
}
