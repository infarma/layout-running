package arquivoDeRetorno

type Cabecalho struct {
	TipoRegistro             int32  `json:"TipoRegistro"`
	CnpjFarmacia             string `json:"CnpjFarmacia"`
	NumeroPedidoFornecedor   string `json:"NumeroPedidoFornecedor"`
	DataProcessamento        int32  `json:"DataProcessamento"`
	CondicaoPagamento        string `json:"CondicaoPagamento"`
	HoraProcessamento        int32  `json:"HoraProcessamento"`
	NumeroPedidoDistribuidor int32  `json:"NumeroPedidoDistribuidor"`
	SituacaoFechamento       int32  `json:"SituacaoFechamento"`
	DataVencimento           int32  `json:"DataVencimento"`
	CnpjDistribuidor         string `json:"CnpjDistribuidor"`
}
