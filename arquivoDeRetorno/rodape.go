package arquivoDeRetorno

type Rodape struct {
	TipoRegistro                int32   `json:"TipoRegistro"`
	NumeroPedido                string  `json:"NumeroPedido"`
	QuantidadeLinhas            int32   `json:"QuantidadeLinhas"`
	QuantidadeItensAntendidos   int32   `json:"QuantidadeItensAntendidos"`
	QuantidadeItensNaoAtendidos int32   `json:"QuantidadeItensNaoAtendidos"`
	LimiteDisponivel            float32 `json:"LimiteDisponivel"`
}
