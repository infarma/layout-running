package arquivoImportacaoPedidosGrandesRedes

type Trailler struct {
	TipoRegistro          int32  `json:"TipoRegistro"`
	NumeroPedido          string `json:"NumeroPedido"`
	QuantidadeLinhas      int32  `json:"QuantidadeLinhas"`
	QuantidadeAtendida    int32  `json:"QuantidadeAtendida"`
	QuantidadeNaoAtendida int32  `json:"QuantidadeNaoAtendida"`
	NumeroNotaFiscal      string `json:"NumeroNotaFiscal"`
	DataEmissaoNota       int32  `json:"DataEmissaoNota"`
}
