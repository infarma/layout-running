package arquivoImportacaoPedidosGrandesRedes

type Lote struct {
	TipoRegistro       int32  `json:"TipoRegistro"`
	EspecieDocumento   string `json:"EspecieDocumento"`
	SerieNotaFiscal    string `json:"SerieNotaFiscal"`
	NumeroNotaFiscal   int32  `json:"NumeroNotaFiscal"`
	NumeroLote         string `json:"NumeroLote"`
	DataVencimentoLote int32  `json:"DataVencimentoLote"`
	QuantidadeLote     int32  `json:"QuantidadeLote"`
}
