package arquivoImportacaoPedidosGrandesRedes

type Itens struct {
	TipoRegistro          int32   `json:"TipoRegistro"`
	CodigoProduto         string  `json:"CodigoProduto"`
	NumeroPedidoIndustria string  `json:"NumeroPedidoIndustria"`
	CodigoPagamento       string  `json:"CodigoPagamento"`
	QuantidadeAtendida    int32   `json:"QuantidadeAtendida"`
	DescontoAplicado      int32   `json:"DescontoAplicado"`
	PrecoUnitario         float32 `json:"PrecoUnitario"`
	PrecoTotalProdutos    float32 `json:"PrecoTotalProdutos"`
	Vago                  string  `json:"Vago"`
	QuantidadeNaoAtendida int32   `json:"QuantidadeNaoAtendida"`
	Motivo                string  `json:"Motivo"`
	CodigoOferta          int32   `json:"CodigoOferta"`
	DescricaoOferta       string  `json:"DescricaoOferta"`
}
