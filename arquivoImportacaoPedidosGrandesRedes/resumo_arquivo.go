package arquivoImportacaoPedidosGrandesRedes

type ResumoArquivo struct {
	TipoRegistro         int32   `json:"TipoRegistro"`
	QuantidadeNotaFiscal string  `json:"QuantidadeNotaFiscal"`
	ValorTotalNotas      float32 `json:"ValorTotalNotas"`
}
