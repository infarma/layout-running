package arquivoImportacaoPedidosGrandesRedes

type Cabecalho struct {
	TipoRegistro             int32  `json:"TipoRegistro"`
	Vago                     string `json:"Vago"`
	CnpjFarmacia             string `json:"CnpjFarmacia"`
	NumeroPedidoIndustria    string `json:"NumeroPedidoIndustria"`
	DataProcessamento        int32  `json:"DataProcessamento"`
	HoraProcessamento        int32  `json:"HoraProcessamento"`
	NumeroPedidoDistribuidor string `json:"NumeroPedidoDistribuidor"`
	Motivo                   string `json:"Motivo"`
	CnpjCDFaturamento        string `json:"CnpjCDFaturamento"`
	NumeroOferta             int32  `json:"NumeroOferta"`
	PrazoOferta              int32  `json:"PrazoOferta"`
	ChaveNotaFiscal          string `json:"ChaveNotaFiscal"`
	DescricaoOferta          string `json:"DescricaoOferta"`
}
