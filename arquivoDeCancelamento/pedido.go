package arquivoDeCancelamento

type Pedido struct {
	TipoRegistro int32  `json:"TipoRegistro"`
	NumeroPedido string `json:"NumeroPedido"`
}
