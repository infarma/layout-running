package arquivoDeCancelamento

type Cliente struct {
	TipoRegistro int32  `json:"TipoRegistro"`
	CnpjCliente  string `json:"CnpjCliente"`
}
