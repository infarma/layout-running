package arquivoDeCancelamento

type Distribuidor struct {
	TipoRegistro     int32  `json:"TipoRegistro"`
	CnpjDistribuidor string `json:"CnpjDistribuidor"`
}
