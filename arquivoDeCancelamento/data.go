package arquivoDeCancelamento

type Data struct {
	TipoRegistro int32  `json:"TipoRegistro"`
	Data         string `json:"Data"`
	Hora         string `json:"Hora"`
}
