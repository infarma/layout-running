package arquivoDeCancelamento

type Itens struct {
	TipoRegistro int32  `json:"TipoRegistro"`
	Vago         string `json:"Vago"`
	CodigoEan    string `json:"CodigoEan"`
	Quantidade   int32  `json:"Quantidade"`
	Motivo       string `json:"Motivo"`
}
