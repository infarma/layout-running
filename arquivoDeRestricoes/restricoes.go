package arquivoDeRestricoes

type Restricoes struct {
	TipoRestricao         int32   `json:"TipoRestricao"`
	DataConsulta          int32   `json:"DataConsulta"`
	CnpjCliente           string  `json:"CnpjCliente"`
	DescricaoDocumentacao string  `json:"DescricaoDocumentacao"`
	ClienteDesde          int32   `json:"ClienteDesde"`
	NumetoTitulo          string  `json:"NumetoTitulo"`
	ValorTitulo           float32 `json:"ValorTitulo"`
	DataVencimento        int32   `json:"DataVencimento"`
	DiasEmAtraso          int32   `json:"DiasEmAtraso"`
	LimiteCredito         float32 `json:"LimiteCredito"`
	ValorAtraso           float32 `json:"ValorAtraso"`
	ValorAcumulado        float32 `json:"ValorAcumulado"`
	DataUltimaCompra      int32   `json:"DataUltimaCompra"`
	ValorMinimoPedido     float32 `json:"ValorMinimoPedido"`
}
