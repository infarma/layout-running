package arquivoNotaFiscal

type Rodape struct {
	TipoRegistro                int32   `json:"TipoRegistro"`
	NumeroPedido                string  `json:"NumeroPedido"`
	QuantidadeLinhas            int32   `json:"QuantidadeLinhas"`
	QuantidadeItensAtendidos    int32   `json:"QuantidadeItensAtendidos"`
	QuantidadeItensNaoAtendidos int32   `json:"QuantidadeItensNaoAtendidos"`
	LimiteDisponivel            float32 `json:"LimiteDisponivel"`
}
