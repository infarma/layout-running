package arquivoNotaFiscal

type Cabecalho struct {
	TipoRegistro             int32  `json:"TipoRegistro"`
	CnpjFarmacia             string `json:"CnpjFarmacia"`
	NumeroPedidoFornecedor   string `json:"NumeroPedidoFornecedor"`
	DataFaturamento          int32  `json:"DataFaturamento"`
	CondicaoFaturamento      string `json:"CondicaoFaturamento"`
	HoraFaturamento          int32  `json:"HoraFaturamento"`
	NumeroPedidoDistribuidor string `json:"NumeroPedidoDistribuidor"`
	SituacaoFechamento       int32  `json:"SituacaoFechamento"`
	DataVencimento           int32  `json:"DataVencimento"`
	CnpjDistribuidor         string `json:"CnpjDistribuidor"`
}
