package arquivoNotaFiscal

type Detalhe struct {
	TipoRegistro                int32   `json:"TipoRegistro"`
	Ean13                       string  `json:"Ean13"`
	NumeroPedido                string  `json:"NumeroPedido"`
	QuantidadeAtendida          int32   `json:"QuantidadeAtendida"`
	DescontoAplicado            float32 `json:"DescontoAplicado"`
	PrazoConcedido              int32   `json:"PrazoConcedido"`
	QuantidadeNaoAtendida       int32   `json:"QuantidadeNaoAtendida"`
	Motivo                      string  `json:"Motivo"`
	NumeroNotaFiscal            string  `json:"NumeroNotaFiscal"`
	SerieNotaFiscal             string  `json:"SerieNotaFiscal"`
	DataEmissaoNotaFiscal       int32   `json:"DataEmissaoNotaFiscal"`
	ChaveAcessoNotaFiscal       string  `json:"ChaveAcessoNotaFiscal"`
	ValorLiquidoFaturado        float32 `json:"ValorLiquidoFaturado"`
	ValorSubstituicaoTributaria float32 `json:"ValorSubstituicaoTributaria"`
}
