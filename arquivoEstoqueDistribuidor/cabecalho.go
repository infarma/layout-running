package arquivoEstoqueDistribuidor

type Cabecalho struct {
	TipoRegistro int32  `json:"TipoRegistro"`
	Ean13        string `json:"Ean13"`
	Quantidade   int32  `json:"Quantidade"`
}
